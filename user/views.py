
from django.shortcuts import render, redirect, get_object_or_404
from accounts.models import User_extra_fields, User
from .forms import UserForm

# user CRUD

def update_users (request,id):
    user_2 =get_object_or_404(User_extra_fields, pk=id)
    form=UserForm(request.POST or None, request.FILES or None, instance=user_2)
    if form.is_valid():
        form.save()
        return redirect('dashboard')
    return render(request, 'form_user.html', {'form_user': form})


def new_users(request):
    form=UserForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        user_form=form.save(commit=False)
        user_form.user_id=request.user.id
        user_form.save()
        form.save_m2m()
        return redirect('dashboard')
    return render(request, 'form_user.html', {'form_user': form})

# user dashboard with many to many fields filtered user_id=id

def user_dash(request,id):

    users=get_object_or_404(User, pk=id)
    user_education  = User_extra_fields.objects.filter(user_id=id)
    user_skill = User_extra_fields.objects.filter(user_id=id)


    if request.method=='POST':
        #job.delete()
        return redirect(new_users)
    return render(request, 'users.html', {'users': users, 'education': user_education,
                                                         'skills': user_skill,})