from django import forms
from django.forms import ModelForm
from accounts.models import User_extra_fields

# form fields with 2 many to many checkbox

class UserForm(ModelForm):
    class Meta:
        model=User_extra_fields
        fields=['education','skills','last_jobs','cv']
        widgets = {
            'education': forms.CheckboxSelectMultiple,
            'skills': forms.CheckboxSelectMultiple,
        }

