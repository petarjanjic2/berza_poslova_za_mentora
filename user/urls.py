from django.urls import path

from .views import update_users, new_users, user_dash

#new and update existing user urls
# url for user dashboard

urlpatterns = [

    path('update/<int:id>/', update_users, name="user_update"),
    path('new/', new_users,  name="user_create"),
    path('users/<int:id>/', user_dash, name="users"),

]