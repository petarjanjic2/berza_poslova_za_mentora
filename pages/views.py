from django.shortcuts import render

from accounts.models import Jobs2
# Create your views here.

# view for home page limited for 3 jobs

def index(request):
    jobs=Jobs2.objects.all().order_by('-list_date').filter(is_published=True)[:3]

    return render(request, 'pages/index.html',  {'job': jobs})