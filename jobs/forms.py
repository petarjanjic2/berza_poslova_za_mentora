from django.forms import ModelForm
from accounts.models import Jobs2
from django import forms

# form for job fields where category is check box for many to many
class JobForm(ModelForm):
    class Meta:
        model=Jobs2

        fields=['title','category', 'address','city','description','number_workers','photo_main']
        widgets = {
            'category': forms.CheckboxSelectMultiple,
        }