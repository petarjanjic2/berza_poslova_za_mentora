from django.urls import path

from .views import list_jobs, new_jobs, update_jobs, delete_jobs, job_dashboard, job_dashboard_user

# urls for jobs CRUD and job dashboard for user and job dashboard for company
urlpatterns = [
    path('', list_jobs, name="jobs_list"),

    path('job_dashboard/<int:id>/', job_dashboard, name="job_dashboard"),
    path('job_dashboard_user/<int:id>/', job_dashboard_user, name="job_dashboard_user"),

    path('new_1/', new_jobs, name="jobs_create"),
    path('update_1/<int:id>/', update_jobs, name="jobs_update"),
    path('delete_1/<int:id>/', delete_jobs, name="jobs_delete"),


]