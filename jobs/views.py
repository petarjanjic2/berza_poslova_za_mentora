from django.shortcuts import render, redirect, get_object_or_404
from accounts.models import User
from accounts.models import Jobs2
from .forms import JobForm
from contacts.models import Contact
from pprint import pprint

#list jobs filtered by admin
def list_jobs(request):
    clients = Jobs2.objects.all().filter(is_published=True, ).order_by('user_id')
    #clients=Jobs2.objects.all().filter (is_published=True, user_id = request.user.id)
    return render(request, 'clients.html', {'clients':clients})

# company can see who sign for job
def job_dashboard (request,id):

    jobs=get_object_or_404(Jobs2, pk=id)
    job_category = Jobs2.objects.filter(id=id)

    user_contacts = Contact.objects.order_by('-contact_date').filter(job_id=id)

    if request.method=='POST':
        #job.delete()
        return redirect(list_jobs)
    return render(request, 'job_dashboard.html', {'job': jobs, 'category': job_category, 'contacts': user_contacts})

def job_dashboard_user (request,id):

    jobs=get_object_or_404(Jobs2, pk=id)
    job_category = Jobs2.objects.filter(id=id)

    if request.method=='POST':
        #job.delete()
        return redirect(list_jobs)
    return render(request, 'job_dashboard_user.html', {'job': jobs, 'category': job_category,})

# CRUD jobs

def new_jobs(request):
    #user=User.objects.all()
    #form=JobForm(request.POST or None, request.FILES or None, initial={'title': 'proba'} )
    form=JobForm(request.POST or None, request.FILES or None)
    if form.is_valid():

        job_form = form.save(commit=False)
        job_form.user_id = request.user.id
        job_form.save()
        form.save_m2m()
        return redirect(list_jobs)
    return render(request, 'form_2.html', {'form_2': form})

def update_jobs (request,id):
    client=get_object_or_404(Jobs2, pk=id)
    form=JobForm(request.POST or None, request.FILES or None, instance=client)
    if form.is_valid():
        form.save()
        return redirect(list_jobs)
    return render(request, 'form_2.html', {'form_2': form})

def delete_jobs(request,id):
    client=get_object_or_404(Jobs2, pk=id)
    if request.method=='POST':
        client.delete()
        return redirect(list_jobs)
    return render(request, 'confirm_1.html', {'client': client})