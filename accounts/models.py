from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime

#user and company fields for registraton,
# fields for diference between user (is_user) and company (is_company)

class User(AbstractUser):

    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)

    company_name = models.CharField(max_length=200, null=True)
    jib = models.CharField(max_length=200, null=True)

    gender = models.BooleanField(default=True)
    date_of_birth = models.DateField(null=True)

    is_company = models.BooleanField(default=False)
    is_user = models.BooleanField(default=False)

# education and skill are many to many fields
# admin in his panel can make inputs for that fields

class Educations(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Skills(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

#extra fields about user and company information

class User_extra_fields(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    education = models.ManyToManyField(Educations, related_name='education')
    skills = models.ManyToManyField(Skills, related_name='skills')
    last_jobs = models.TextField(max_length=200)

    cv = models.FileField(upload_to='documents/%Y/%m/%d/')

    def __str__(self):
        return self.user.username


class Company_extra_fields(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,)


    photo = models.ImageField(upload_to='photos/%Y/%m/%d/')
    purpose = models.TextField(blank=True)
    activities = models.TextField(blank=True)
    phone = models.CharField(max_length=20)
    staff = models.IntegerField(default=1)

    def __str__(self):
        return self.purpose

class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

#job fields in database

class Jobs2(models.Model):


    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ManyToManyField(Category, related_name='category')
    title = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    number_workers = models.IntegerField()

    photo_main = models.ImageField(upload_to='photos/%Y/%m/%d/')

    is_published = models.BooleanField(default=False)
    list_date = models.DateTimeField(default=datetime.now, blank=True)


    def __str__(self):
        return self.title