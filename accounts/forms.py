from django import forms
from .models import User

from django.contrib.auth.forms import UserCreationForm

#form and fields for register user

class UsersRegisterForm (UserCreationForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'gender', 'date_of_birth', 'username', 'email', 'password1', 'password2']

#form and fields for register company

class CompanysRegisterForm (UserCreationForm):

    class Meta:
        model = User
        fields = ['company_name','jib','username', 'email', 'password1', 'password2']