from django.urls import path
from django.contrib.auth.views import LogoutView
from django.conf import settings

from . import views

#use django logout redirect

urlpatterns = [
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('register_company', views.register_company, name='register_company'),

    path('logout', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('dashboard_company', views.dashboard, name='dashboard_company'),


]
