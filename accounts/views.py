from django.shortcuts import render, redirect
from django.contrib import messages, auth
#from django.contrib.auth.models import User
from contacts.models import Contact
from django.contrib.auth import get_user_model
from .forms import CompanysRegisterForm, UsersRegisterForm
User = get_user_model()
from .models import Jobs2, User_extra_fields

#register forms
# hiden input in database is_user or is_company registered

def register(request):
  if request.method == 'POST':
    form = UsersRegisterForm(request.POST)
    if form.is_valid():
      user =form.save(commit=False)
      user.is_user = True
      user.save()
      messages.success(request, 'You are now registered and can log in')
      return redirect('login')
    else:
      messages.error(request, 'Wrong input')
  else:
    form = UsersRegisterForm()
  return render(request, 'accounts/register.html', {'form': form})


def register_company(request):
  if request.method == 'POST':
    form = CompanysRegisterForm(request.POST)
    if form.is_valid():
      user =form.save(commit=False)
      user.is_company = True
      user.save()
      messages.success(request, 'You are now registered and can log in')
      return redirect('login')
    else:
      messages.error(request, 'Wrong input')
  else:
    form = CompanysRegisterForm()
  return render(request, 'accounts/register_company.html', {'form': form})

def login(request):
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']

    user = auth.authenticate(username=username, password=password)

    if user is not None:
      auth.login(request, user)
      messages.success(request, 'You are now logged in')
      return redirect('dashboard')
    else:
      messages.error(request, 'Invalid credentials')
      return redirect('login')
  else:
    return render(request, 'accounts/login.html')

#dashboard for user and company

def dashboard(request):
  if request.user.is_authenticated:
    if request.user.is_company:
      jobs = Jobs2.objects.all().filter(user_id=request.user.id)
      return render(request, 'accounts/dashboard_company.html', {'job': jobs})
    else:
      user_education = User_extra_fields.objects.filter(user_id=request.user.id)
      user_skill = User_extra_fields.objects.filter(user_id=request.user.id)
      user_contacts = Contact.objects.order_by('-contact_date').filter(user_id=request.user.id)
      return render(request, 'accounts/dashboard.html', {'contacts': user_contacts, 'education': user_education,
                                                         'skills': user_skill,})

  return render(request, 'accounts/dashboard.html')




