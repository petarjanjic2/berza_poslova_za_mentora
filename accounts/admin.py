from django.contrib import admin
from .models import User, User_extra_fields, Company_extra_fields, Jobs2, Educations, Skills, Category

#admin can publish job with comand list_editable

class JobsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'is_published', 'number_workers', 'list_date', 'user')
    list_display_links = ('id', 'title')
    list_editable = ('is_published',)
    list_filter = ('user',)

    search_fields = ('title',)
    list_per_page = 25

#view fields in admin dashboard

admin.site.register(User)
admin.site.register(User_extra_fields)
admin.site.register(Company_extra_fields)
admin.site.register(Jobs2, JobsAdmin)
admin.site.register(Educations)
admin.site.register(Skills)
admin.site.register(Category)