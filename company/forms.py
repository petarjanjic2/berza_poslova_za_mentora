
from django.forms import ModelForm
from accounts.models import Company_extra_fields

# fields for company CRUD

class CompanyForm(ModelForm):
    class Meta:
        model=Company_extra_fields
        fields=['photo','purpose','activities','phone','staff',]
