from django.urls import path

from .views import list_companys, company_dashboard, update_companys, new_companys

# urls for company CRUD, company dashboard and list companys

urlpatterns = [
    path('', list_companys, name="companys_list"),
    path('update_2/<int:id>/', update_companys, name="company_update"),
    path('new_2/', new_companys, name="companys_create"),


    path('company_dashboard/<int:id>/', company_dashboard, name="company_dashboard"),

]