
from django.shortcuts import render, redirect, get_object_or_404
from accounts.models import User
from accounts.models import Company_extra_fields
from .forms import CompanyForm


# Create your views here.
# list companys

def list_companys(request):
    company = User.objects.all()
    return render(request, 'companys.html', {'company': company})

# company dashboard
def company_dashboard(request, id):
    company_1 = get_object_or_404(User, pk=id)
    if request.method == 'POST':
        # job.delete()
        return redirect(list_companys)
    return render(request, 'company_dashboard.html', {'company_1': company_1})

# update company extra fields
def update_companys (request,id):
    client_2 =get_object_or_404(Company_extra_fields, pk=id)
    form=CompanyForm(request.POST or None, request.FILES or None, instance=client_2)
    if form.is_valid():
        form.save()
        return redirect('dashboard_company')
    return render(request, 'form_1.html', {'form_1': form})

# new company extra fields with hiden input for user
def new_companys(request):
    form=CompanyForm(request.POST or None, request.FILES or None, initial={'user': 2})
    if form.is_valid():
        user_form = form.save(commit=False)
        user_form.user_id = request.user.id
        user_form.save()
        return redirect('dashboard_company')
    return render(request, 'form_1.html', {'form_1': form})