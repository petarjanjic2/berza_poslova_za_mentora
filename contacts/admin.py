from django.contrib import admin

from .models import Contact

# showing user contacts to companys jobs

class ContactAdmin(admin.ModelAdmin):
  list_display = ('id', 'name', 'job', 'email', 'contact_date')
  list_display_links = ('id', 'name')
  search_fields = ('name', 'email', 'job')
  list_per_page = 25

admin.site.register(Contact, ContactAdmin)
